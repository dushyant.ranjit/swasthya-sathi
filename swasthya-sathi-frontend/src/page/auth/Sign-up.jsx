import { Button, PasswordInput, Select, Text, TextInput } from "@mantine/core";
import axios from "axios";
import { useState } from "react";
import img1 from "../../assets/images/signupp.png"
import { NavLink } from "react-router-dom";
export const SignupPage=()=>{
    const [signup, setSignup]=useState({
        fullName:"",
        phone:"",
        email:"",
        password:"",
    })
    const handleInput=(event) =>{
        setSignup({
            ...signup,
            [event.target.name]:event.target.value,
        });
      };

    const signUp=async (event)=>{
        event.preventDefault();
        console.log(signup)
        const dataCreated=await axios.post("http://localhost:8081/auth/sign-up",signup);
        console.log(dataCreated);
        
    }
    return<>
    <section className="flex justify-center h-screen" >
     <div className="auth-layout flex items-center justify-evenly m-32">
     {/* <div className="image-area font-sans text-2xl w ">
            Create a
            n account
        </div> */}
        <img src={img1} alt="create"/>
        <div className="form-area">
        <form onSubmit={signUp}>
        <div className="flex justify-evenly space-x-5">
        <div className="flex-col">
            <label>Full Name:</label>
         <div className="form-box ">  
            <input type="text" name="fullName" onChange={handleInput} placeholder="Enter fullname"/>
         </div>
        </div> 
        <div className="flex-col ">
        <label>Phone no.:</label>
         <div className="form-box">  
        <input type="text" name="phone" onChange={handleInput} placeholder="Enter phone number"/>
        </div>
        </div>
        </div>
       <div className="flex justify-evenly space-x-5">
        <div className="flex-col">
        <label>Email:</label>
         <div className="form-box">  
        <input type="text" name="email" onChange={handleInput} placeholder="Enter fullname"/>
        </div>
        </div>
            <div className="flex-col">
            <label>Password:</label>
            <div className="form-box">  
            <input type="password" name="password" onChange={handleInput} placeholder="Enter fullname"/>
            </div>
        </div>
       </div>
        {/* Fullname:{""} */}

       {/* <TextInput type="text" value= {signup.username}name="username" onChange={handleInput} /> */}
    
       {/* Phone No.:{""} 
       <TextInput type="text" value= {signup.phone}name="phone" onChange={handleInput} />
       
       Email:{""}
       <TextInput type="text" value= {signup.email}name="email" onChange={handleInput} />
       
       Password:{""}
       <PasswordInput type="text" value= {signup.password}name="password" onChange={handleInput} /> */}

      <div className="flex justify-center">
        <Select
        label="Role"
        // placeholder="User"
        // defaultChecked="User"
        defaultValue="User"
        data={['User','Doctor']}
        />
      </div>
       <br/>
       <div className="flex justify-center">
       <Button type="submit" >Create Account</Button>

       </div>
       <br/>
       <div className="flex justify-center">
       Already have an account? Please  <NavLink to='/auth/login' style={{color:'blue', textDecoration:'underline'}}> login.</NavLink>

       </div>
        </form>
        </div>
     </div>
    </section>
    </>
}