import { useState } from "react"
import axios from 'axios';
import logo from '../../assets/images/logofull - Copy.png';
import { PostRequest } from "../../plugins/https";
import { setTokenToLocalStorage } from "../../utils/localstorage.helper";
import { setAuthorizationHeader } from "../../utils/auth.helper";
import { NavLink } from "react-router-dom";
export const Login =()=>{
    const [formData, setformData ] = useState({
        email:'',
        password:''
    });

    const inputHandler = (e)=>{
        setformData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const submitForm = async (e)=>{
        e.preventDefault();
        const res =await PostRequest('auth/login',{
            email: formData.email,
            password: formData.password,
        });
        // console.log(res.data.token);
        
        const { token, user } = res.data;
        setTokenToLocalStorage(res.data.token);  // localStorage.setItem('token',res.data.token);
        setAuthorizationHeader(res.data.token);  // localStorage.setItem('token',res.data.token);
        localStorage.setItem('user', JSON.stringify(user)); // Store user info in localStorage

        // console.log(res.data.token);
        window.location.reload();
        
        
    }

    return <section className="flex justify-center h-screen " >
     <div className="auth-layout flex items-center justify-evenly m-40 mt-24">
     <div className="image-area">
            <img src={logo} alt="" />
        </div>
        <div className="form-area ">
        <form onSubmit={submitForm}>
        <div className="auth-title">Login</div>

            <div className="form-box">
                <label>Email</label>
                <input type="text" name="email" onChange={inputHandler} placeholder="Enter email"/>
            </div>
            <div className="form-box">
                <label>Password</label>
                <input type="password" name="password" onChange={inputHandler} placeholder="Enter password"/>
            </div>
            <div className="forget-password">Forget Password</div>
           <div className="btn-area">
           <button type="submit" className="btn primary">Login</button>
           </div>

            <div className="sign-up-link">
                Don't have an account? Please <NavLink to="/signup" style={{color:'blue', textDecoration:'underline'}}>sign up.</NavLink>
            </div>
        </form>
        </div>
     </div>
    </section>
}