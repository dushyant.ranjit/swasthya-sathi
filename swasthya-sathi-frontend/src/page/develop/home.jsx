import { Button, Text } from "@mantine/core";
import img1 from '../../assets/images/home.jpg'
import { Navbar } from "../../components/partials/navbar/Navbar";
import { NavLink } from "react-router-dom";

export const Home=()=>{
    return<>
    <div className="homepage flex justify-between h-screen space-x-7">
        <div className="text-area flex-col justify-center  items-center font-sans px-5 py-40 ">
            <div className="title-home  flex items-center  bold py-3 ">
                <Text
                 component="span"
                inherit
                variant="gradient"
                gradient={{ from: 'blue', to: 'grey' }}
                >   Swasthya Sathi</Text>
            </div>
                <div className="title-description flex items-center justify-between"> 
                        <Text  className="description">
                            Swasthya Sathi is a web application designed to digitally store and manage your hospital reports. It enables seamless sharing of reports with your doctors during appointments, making it easier to access, organize, and carry your medical records.
                            <p> Swasthya Sathi aims to revolutionize how individuals manage and share their health records. By providing a secure, easy-to-use, and comprehensive platform, we aim to make healthcare more efficient and accessible for everyone.</p>
                            <br/>
                    </Text></div>
                <div className="flex items-center"><Button color="#0480C4"><NavLink to="/auth/signup">Get started</NavLink></Button></div>
        
        </div>
        <div className="image">
            <img src={img1} alt="logo"/>


        </div>
    </div>
    </>
}