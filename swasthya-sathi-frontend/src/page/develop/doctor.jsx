import { Text, Card, RingProgress, Group, useMantineTheme } from '@mantine/core';
import classes from '../../assets/css/StatsRingCard.module.css';

const stats = [
  { value:55, label: 'Remaining' },
  { value: 15, label: 'Checked' },
];
const state = [
    { value:7, label: 'Remaining' },
    { value: 8, label: 'Arrived' },
  ];

export const Doctor=()=>{
    const theme = useMantineTheme();
    const completed = 15;
    const total = 70;
    const items = stats.map((stat) => (
      <div key={stat.label}>
        <Text className={classes.label}>{stat.value}</Text>
        <Text size="xs" c="dimmed">
          {stat.label}
        </Text>
      </div>
    ));
    const complete = 8;
    const totally = 15;
    const itemss = state.map((staty) => (
      <div key={staty.label}>
        <Text className={classes.label}>{staty.value}</Text>
        <Text size="xs" c="dimmed">
          {staty.label}
        </Text>
      </div>
    ));
    return<>
    <div className="flex-col ">
        <div className="flex justify-center space-x-10 ">
        <Card withBorder p="xl" radius="md" className={classes.card}>
      <div className={classes.inner}>
        <div>
          <Text fz="xl" className={classes.label}>
             Patient  Data
          </Text>
          <div>
            <Text className={classes.lead} mt={30}>
              70
            </Text>
            <Text fz="xs" c="dimmed">
              Total patient
            </Text>
          </div>
          <Group mt="lg">{items}</Group>
        </div>

        <div className={classes.ring}>
          <RingProgress
            roundCaps
            thickness={6}
            size={150}
            sections={[{ value: (completed / total) * 100, color: theme.primaryColor }]}
            label={
              <div>
                <Text ta="center" fz="lg" className={classes.label}>
                  {((completed / total) * 100).toFixed(0)}%
                </Text>
                <Text ta="center" fz="xs" c="dimmed">
                  Completed
                </Text>
              </div>
            }
          />
        </div>
      </div>
    </Card>
    <Card withBorder p="xl" radius="md" className={classes.card}>
      <div className={classes.inner}>
        <div>
          <Text fz="xl" className={classes.label}>
            Appointment
          </Text>
          <div>
            <Text className={classes.lead} mt={30}>
              15
            </Text>
            <Text fz="xs" c="dimmed">
              Total appointment
            </Text>
          </div>
          <Group mt="lg">{itemss}</Group>
        </div>

        <div className={classes.ring}>
          <RingProgress
            roundCaps
            thickness={6}
            size={150}
            sections={[{ value: (complete / totally) * 100, color: theme.primaryColor }]}
            label={
              <div>
                <Text ta="center" fz="lg" className={classes.label}>
                  {((complete / totally) * 100).toFixed(0)}%
                </Text>
                <Text ta="center" fz="xs" c="dimmed">
                  Completed
                </Text>
              </div>
            }
          />
        </div>
      </div>
    </Card>
        </div>
        <div></div>
    </div>
    </>
}