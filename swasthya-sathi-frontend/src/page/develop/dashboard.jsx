import { Button, Card,Paper,Text, TextInput, ThemeIcon } from "@mantine/core"
import { IconDownload, IconReport, IconReportMedical, IconStethoscope, IconX } from "@tabler/icons-react";
import { Dialog } from "../../components/common/Dialog";
import { useState } from "react";
import { UserDetail } from "../../components/modules/user-dashboard/UserDetail";
import { NavLink } from "react-router-dom";
import { AddNote } from "../../components/modules/user-dashboard/file-work/AddNote";
// import { Navbar } from "../../components/partials/Navbar";



export const Dashboard=()=>{
  const [content, setContent] = useState(false);
  const showContent=()=>{
    setContent(!content);
}
  const [add, setAdd]= useState(false);
  const showAdd=()=>{
    setAdd(!add);
  }
  const [Report, setReport]= useState(false);
  const showReport=()=>{
    setReport(!Report);
  }


  const addDoctor=async (event)=>{
    event.preventDefault();
    // console.log(signup)
    // const dataCreated=await axios.post("http://localhost:8081/auth/sign-up",signup);
    console.log(dataCreated);
    
}

return<>
  <div className="dashboard-page flex space-x-12">
    <UserDetail/>
    <div className="right-area">
      <div className="right-header">
        <Card withBorder p="xl" radius="md" style={{ backgroundColor: "rgba(179, 213, 242, 0.5)" }}>
          <div className="text-area  font-serif ">
          <Text pb={10} size='40px' fw={500} >Swasthya Sathi</Text>
          </div>
          <div className="font-serif">
            <Text size="26px">Your Personal Health Companion</Text>
          </div>
        </Card>
      </div>
      <div className="button-area flex-col py-32">
        <div className="divide flex justify-around  space-x-4">
        {/* <div className="flex justify-center w-70"> */}
        <Card withBorder p="xl" radius="md" style={{backgroundColor:"rgb(179, 213, 242)"}} className="cursor-pointer h-[30vh] flex items-center justify-center w-[15vw]">
        {/* <Button variant="outline" color="#0480C4" size="80px" height="80px" > */}
            <NavLink to="/file/file_1719110341341.pdf">
            <div className="flex flex-col justify-center  items-center space-x-3">
          <Text>View My report  </Text>
            {/* <ThemeIcon size={70}> */}
              <IconReport size={70} color="#0480C4"/>
            {/* </ThemeIcon>   */}
            </div> 
              </NavLink> 
          {/* </Button> */}
          </Card>
        {/* </div> */}
        {/* <div className="divide flex justify-center py-32 space-x-36"> */}

          <Card withBorder p="xl" radius="md" style={{backgroundColor:"rgb(179, 213, 242)"}} className="cursor-pointer h-[30vh] flex items-center justify-center w-[15vw]">
            
            {/* <Button  variant="outline" color="#0480C4" size="80px" onClick={()=>setContent(true)}> */}
            <div className="flex flex-col justify-center  items-center space-x-3">
              <Text>Add new report  </Text>
              {/* <ThemeIcon> */}
                <IconReportMedical size={70} color="#0480C4"/>
              {/* </ThemeIcon>   */}
            </div> 
          {/* </Button> */}
          </Card>
          
        
          <Card withBorder p="xl" radius="md" style={{backgroundColor:"rgb(179, 213, 242)"}} className="cursor-pointer h-[30vh] flex items-center justify-center w-[15vw]">
            
            {/* <Button  variant="outline" color="#0480C4" size="80px" onClick={()=>setAdd(true)}> */}
            <div className="flex flex-col justify-center  items-center space-x-3">
              <Text>Add doctor  </Text>
              {/* <ThemeIcon> */}
                <IconStethoscope size={70} color="#0480C4"/>
              {/* </ThemeIcon>   */}
            </div>  
          {/* </Button> */}
          </Card>
        </div>
      </div>
    </div>
  </div>
  <Dialog show={content} close={()=>setContent(false)}>
      <AddNote/>
          </Dialog>
  <Dialog show={add} close={()=>setAdd(false)}>
          Add doctor
          <form className="py-3" >
          {/* <form className="py-3" onSubmit={addDoctor}> */}
            <label>E-mail:</label>
            <TextInput type="text"  name="email" />
            <Button  variant="outline" color="#0480C4" type="submit">Add</Button>
          </form>

          </Dialog>        
</>
}