import { Container, Title, Text, Button } from '@mantine/core';
import classes from '../../assets/css/HeroBullets.module.css';
import { NavLink } from 'react-router-dom';

export function HeroBullets() {
  return (
    <div className={classes.root}>
      <Container size="lg">
        <div className={classes.inner}>
          <div className={classes.content}>
            <Title className={classes.title}>
              <Text
                component="span"
                inherit
                variant="gradient"
                gradient={{ from: 'blue', to: 'grey' }}
                size='xl'
              >
                  Who Are We?
              </Text>
            </Title>

            <Text  mt={30} c='black' >
           
            We are a team of enthusiastic computer engineering students committed to using technology to address and simplify everyday challenges.
            <p>Our goal is to simplify and enhance the way people manage their health records through innovative digital solutions.</p>

            </Text>
            <br/>
            <Title className={classes.title}>
              <Text
                component="span"
                inherit
                variant="gradient"
                gradient={{ from: 'blue', to: 'grey' }}
                size='xl'
              >
                  What Our Website Does?
              </Text>
            </Title>

            <Text  mt={30} c='black' >
           
            Our web application is designed to streamline the storage and access of your medical reports. With our platform, you can securely store all your medical documents in one place, ensuring you never have to search through your house before a doctor's appointment.
            <p>Additionally, our service allows you to effortlessly share your reports with your healthcare providers, making your medical consultations smoother and more efficient.</p>

            </Text>

            <Button
              variant="gradient"
              gradient={{ from: 'blue', to: '#0480C4' }}
              size="l"
              className={classes.control}
              mt={40}
            >
             <NavLink to="/auth/signup"> Get started</NavLink>
            </Button>
          </div>
        </div>
      </Container>
    </div>
  );
}