import { Button, Card, Text } from "@mantine/core";
import { useEffect, useState } from "react";
import { GetRequest } from "../../../plugins/https";

export const UserDetail = () => {
    const formatDate = (dateString) => {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(dateString).toLocaleDateString(undefined, options);
      };
  const [userData, setUserData] = useState(null);
  const user = JSON.parse(localStorage.getItem('user'));

  const getAllSubject = async () => {
    const res = await GetRequest(`/userdetails`);
    const filtered = res.data.find(fetched => fetched.user === user._id);
    setUserData(filtered);
  };

  useEffect(() => {
    if (user?._id) {
      getAllSubject();
    }
  }, [user?._id]);

  return (
    <div className="my-detail h-[80vh] w-80 flex items-center px-8">
      <Card withBorder p="xl" radius="md" className="h-[40vh] flex-col justify-center">
        <div className="flex text-area text-xl">
          {/* <Text size=""> */}
             My details
             {/* </Text> */}
        </div>
        {userData ? (
          <div className="my-5">
            <Text>Date of Birth: {formatDate(userData.dob)}</Text>
            <Text>Height: {userData.height}</Text>
            <Text>Weight: {userData.weight}</Text>
            <Text>Gender: {userData.gender}</Text>
            <Text>Blood Group: {userData.bloodGroup}</Text>
          </div>
        ) : (
          <Text>Loading...</Text>
        )}
        <Button Button color='#0480C4' >Update</Button>
      </Card>
    </div>
  );
};
