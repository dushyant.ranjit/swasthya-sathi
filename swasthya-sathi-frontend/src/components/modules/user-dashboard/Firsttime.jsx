import { Button } from "@mantine/core"
import { NavLink, Navigate } from "react-router-dom"

export const Firstvisit=()=>{
    return<>
    <div className="description flex justify-center items-center h-screen">
        <div className="flex-col justify-center items-center">
        <div className="title  font-sans text-6xl bold flex justify-center ">Welcome to Swastya Sathi</div>
        <div className="flex justify-center py-5">  <Button colour="#0480C4" radius={20}>
            <NavLink to="/detailsform">Get started with our feature</NavLink>
            </Button></div>
    </div>
        </div>
    </>
}