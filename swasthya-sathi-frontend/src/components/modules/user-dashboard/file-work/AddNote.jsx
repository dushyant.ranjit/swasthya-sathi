import React from "react";
import { useState } from "react";
import { Text, Image, SimpleGrid, TextInput, Button, Select, Container, Center, Box, ThemeIcon } from "@mantine/core";
import { Dropzone, IMAGE_MIME_TYPE, PDF_MIME_TYPE } from "@mantine/dropzone";
import axios from "axios";
import { PostRequest } from "../../../../plugins/https";
import { IconFileUpload } from "@tabler/icons-react";
import { useNavigate } from "react-router";

export const AddNote = () => {
  const [files, setFiles] = useState([]);
  const [blog, setBlog] = useState({
    title: "",
  });
  const navigate = useNavigate();

  const handleInput = (event) => {
    setBlog({
      ...blog,
      [event.target.name]: event.target.value,
    });
  };

  const previews = files.map((file, index) => {
    const imageUrl = URL.createObjectURL(file);
    return (
      <Image
        key={index}
        src={imageUrl}
        onLoad={() => URL.revokeObjectURL(imageUrl)}
      />
    );
  });

  const convertToFormData = (obj) => {
    const formData = new FormData();
    for (const key in obj) {
      formData.append(key, obj[key]);
    }
    return formData;
  };

  const addBlog = async (event) => {
    event.preventDefault();
    console.log("checkk");

    const formData = convertToFormData(blog);
    formData.append("file", files[0]);

    const res =await PostRequest('file/upload',formData);
    // const res = await axios.post("http://localhost:8091/file/upload",formData);
    console.log(res);
    const user = JSON.parse(localStorage.getItem('user'));
    // console.log(user.email);
    const fileUploadData={
        title:blog.title,
        path:res.filename,
        user:user._id,
        // noteTitle:blog.title,
        // pdfPath:res.filename,
        // subject:"6675203fc0b6b9fb6c962db4"
    }
    const resp =await PostRequest('userdocs',fileUploadData);
    console.log(resp);
    navigate("/");
  };
    return<>
        <div className="addnote flex justify-center m-12  sm-3xl p-3xl h-screen">
      <form action="" className="flex-col gap-sm" onSubmit={addBlog}>
        <TextInput label="Title" placeholder="title" name="title" onChange={handleInput} value={blog.title} />
        
        {/* <TextInput label="semester" placeholder="semester" name="semester" onChange={handleInput} value={blog.semester} /> */}
        
        <div  className=" m-4">
          <Dropzone accept={PDF_MIME_TYPE} onDrop={setFiles} maxFiles={1}>
            <ThemeIcon>
              <IconFileUpload/>
            </ThemeIcon>
            {/* <Text ta="center"style={{color:'blue', textDecoration:'underline'}} >Drop your pdf file here</Text> */}
          </Dropzone>

          <SimpleGrid
            cols={{ base: 1, sm: 4 }}
            mt={previews.length > 0 ? "xl" : 0}
          >
            {previews}
          </SimpleGrid>
        </div >
        <Button type="submit" variant="outline" color='#7359a7'>Submit</Button>
      </form>
    </div>
    </>
}