import React from 'react';
import { Viewer, Worker } from '@react-pdf-viewer/core';
import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout';
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';
import { useLocation } from 'react-router';

export const FileView = () => {
    const defaultLayoutPluginInstance = defaultLayoutPlugin();
    const location = useLocation();
    const urlParams = location.pathname.split('/');
    const filename = urlParams[urlParams.length - 1];


    const fileUrl = `http://localhost:8081/file/${filename}`;
    return (
        <div className="file-view-container">
            <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.4.120/build/pdf.worker.min.js">
            {/* <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.4.120/build/pdf.worker.min.js"> */}
                <Viewer
                    fileUrl={fileUrl}
                    plugins={[defaultLayoutPluginInstance]}
                />
            </Worker>
        </div>
    );
};
