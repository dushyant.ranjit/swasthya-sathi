import {  Radio, Group,  Select } from "@mantine/core"
import axios from "axios";
import { useState } from "react"
import { useNavigate } from "react-router";

export const Firstform=()=>{
    const user = JSON.parse(localStorage.getItem('user'));
    const navigate = useNavigate();
    const [formData, setFormData ] = useState({
        user: user ? user._id : '',
        dob:'',
        height:'',
        weight:'',
        bloodGroup:'',
        gender:'',
    });

    const inputHandler = (e)=>{
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSelectChange = (value) => {
        setFormData((prevData) => ({
            ...prevData,
            bloodGroup: value,
        }));
    };

    const handleRadioChange = (value) => {
        setFormData((prevData) => ({
            ...prevData,
            gender: value,
        }));
    };

    const updateDetails=async (event)=>{
        event.preventDefault();
        const dataCreated=await axios.post("http://localhost:8081/userdetails",formData);
        console.log(dataCreated);
        user.hasDetails=true;
        const firstDataLoged=await axios.put(`http://localhost:8081/user/${user._id}`,user);
        console.log(firstDataLoged);
        localStorage.setItem('user', JSON.stringify(user));
        window.location.reload();
    }
    return<>
    
    <section className=" cover flex justify-center" >
     <div className="first-layout flex items-center justify-evenly">
    
        <div className="form-area ">
        <form on onSubmit={updateDetails}>
        <div className="auth-title">Fill your details</div>
                <label>Date of birth</label>
            <div className="form-box">
                <input type="date" name="dob" onChange={inputHandler} placeholder="Enter D.O.B"/>
            </div>
                <label>Height</label>
            <div className="form-box">
                <input type="text" name="height" onChange={inputHandler} placeholder="Enter height"/>
            </div>
                <label>Weight in kg</label>
            <div className="form-box">
                <input type="number" name="weight" onChange={inputHandler} placeholder="Enter weight"/>
            </div>
            <Radio.Group
                    name="gender"
                    label="Gender"
                    onChange={handleRadioChange}
                    >
                <Group mt="xs">
                    <Radio  value="Male" label="Male" />
                    <Radio value="Female" label="Female" />
                    <Radio value="Other" label="Other" />
                </Group>
            </Radio.Group>
            <Select 
                label="Blood Group"
                placeholder="Choose your blood group"
                data={['A+ve','B+ve','AB+ve','O+ve','A-ve','B-ve','AB-ve','O-ve']}
                onChange={handleSelectChange}
            />
           <div className="btn-area">
           <button type="submit" className="btn primary">Save</button>
           </div>
        </form>
        </div>
     </div>
    </section>
    </>
}