import {
    HoverCard,Group,Button,UnstyledButton,Text,SimpleGrid,ThemeIcon,Anchor,Divider,Center,Box,Burger,Drawer,Collapse,ScrollArea,rem,useMantineTheme,
  } from '@mantine/core';
  import { MantineLogo } from '@mantinex/mantine-logo';
  import { useDisclosure } from '@mantine/hooks';
  import {
    IconNotification, IconCode, IconBook, IconChartPie3, IconFingerprint, IconCoin, IconChevronDown,
  } from '@tabler/icons-react';
  import classes from './NavbarCSS.module.css';
  import logo from '../../../assets/images/logo.png';
import { NavLink } from 'react-router-dom';
import { logout } from '../../../utils/auth.helper';
  // import '../../assets/css/style.css'
  
 
  export function UserNav() {
    const [drawerOpened, { toggle: toggleDrawer, close: closeDrawer }] = useDisclosure(false);
    const [linksOpened, { toggle: toggleLinks }] = useDisclosure(false);
    const theme = useMantineTheme();
  
  
    return (
      <Box pb={0}>
        <header className={classes.header}>
          <Group justify="space-between" h="100%">
          <img src={logo} alt="Logo" className='logo'/>
          
              <a href="#" className={`${classes.link} red-link`}>
             Dial- 102 (for emergency)
              </a>
          
  
            <Group h="100%" gap={0} visibleFrom="sm">
              <a href="#" className={classes.link} >
                < NavLink to="/">Home</NavLink>
              </a>
              <a href="#" className={classes.link}>
                <NavLink to="/about">About Us</NavLink>
              </a>
              <a href="#" className={classes.link}>
                <NavLink to="/contact">Contact Us</NavLink>
              </a>
            </Group>
  
            <Group visibleFrom="sm">
              {/* <Button  variant="outline" color="#0480C4"><NavLink to="/login">Log in</NavLink> </Button> */}
              <Button color='#0480C4' onClick={logout}>
                {/* <NavLink to="/signup"> */}
                Sign Out
                {/* </NavLink> */}
                </Button>
            </Group>
  
            <Burger opened={drawerOpened} onClick={toggleDrawer} hiddenFrom="sm" />
          </Group>
        </header>
  
        <Drawer
          opened={drawerOpened}
          onClose={closeDrawer}
          size="100%"
          padding="md"
          title="Navigation"
          hiddenFrom="sm"
          zIndex={1000000}
        >
          <ScrollArea h={`calc(100vh - ${rem(80)})`} mx="-md">
            <Divider my="sm" />
  
            <a href="#" className={classes.link}>
              Home
            </a>
            <UnstyledButton className={classes.link} onClick={toggleLinks}>
              <Center inline>
                <Box component="span" mr={5}>
                  Features
                </Box>
                <IconChevronDown
                  style={{ width: rem(16), height: rem(16) }}
                  color={theme.colors.blue[6]}
                />
              </Center>
            </UnstyledButton>
            <Collapse in={linksOpened}>
              {/* {links} */}
            </Collapse>
            <a href="#" className={classes.link}>
              Learn
            </a>
            <a href="#" className={classes.link}>
              Academy
            </a>
  
            <Divider my="sm" />
  
            <Group justify="center" grow pb="xl" px="md">
              {/* <Button variant="default">Log in</Button>
              <Button>Sign up</Button> */}
            </Group>
          </ScrollArea>
        </Drawer>
      </Box>
    );
  }