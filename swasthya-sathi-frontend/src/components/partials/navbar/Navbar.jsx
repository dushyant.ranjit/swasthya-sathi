import { Route, Routes } from "react-router"
import { useDispatch, useSelector } from 'react-redux'
import { GuestNav } from "./GuestNav.jsx"
import { UserNav } from "./UserNav"
import { getTokenFromLocalStorage } from "../../../utils/localstorage.helper"
import { setToken } from "../../../store/modules/auth/action.js"
// import { setToken } from "./store/modules/auth/action";

export const Navbar=()=>{
  const token = useSelector((state) => state.tokenReducer.token);
  console.log("Token is ", token);
  const dispatch = useDispatch();
  const isLoggedIn = getTokenFromLocalStorage();
  console.log(isLoggedIn);
  if (isLoggedIn) {
    dispatch(setToken(isLoggedIn));
  }
  const isUser = () => {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    return user && user.role === 'default';
  };

  return<>
    <Routes>
      <Route path="/*" element={token == "" ? <GuestNav /> : <UserNav/>} />
    </Routes>
  </>
}