import { Navigate, Route, Routes } from "react-router"
import { Login } from "../page/auth/Login"
import { getTokenFromLocalStorage } from "../utils/localstorage.helper"
import {SignupPage} from "../page/auth/Sign-up.jsx"
import { Home } from "../page/develop/Home.jsx"
import { Dashboard } from "../page/develop/Dashboard.jsx"
import { HeroBullets } from "../page/develop/Aboutus.jsx"
import { GetInTouchSimple } from "../page/develop/Contact.jsx"
import { Doctor } from "../page/develop/Doctor.jsx"
import { useDispatch, useSelector } from "react-redux"
import { setToken } from "../store/modules/auth/action.js"
import { Firstvisit } from "../components/modules/user-dashboard/Firsttime.jsx"
import { Firstform } from "../components/modules/user-dashboard/Firstform.jsx"
import { AddNote } from "../components/modules/user-dashboard/file-work/AddNote.jsx"
import { FileView } from "../components/modules/user-dashboard/file-work/FileView.jsx"

export const AppRoutes = ()=>{
    const token = useSelector((state) => state.tokenReducer.token);
  console.log("Token is ", token);
  const dispatch = useDispatch();
  const isLoggedIn = getTokenFromLocalStorage();
  if (isLoggedIn) {
    dispatch(setToken(isLoggedIn));
  }
  const isUser = () => {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    console.log(user && user.role === 'default');
    return user && user.role === 'default';
  };
  const firstTime = () => {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    console.log(user.hasDetails);
    return !user.hasDetails;
  };

    return <Routes>
        <Route path="/" element={token == "" ? <Home /> : <Navigate to="/user" />} />
        <Route path="/login"element={token == "" ? <Login /> : <Navigate to="/" />} />
        <Route path="/signup" element={token == "" ? <SignupPage /> : <Navigate to="/" />} />
        <Route path="/file" element={!token == "" ? <AddNote /> : <Navigate to="/" />} />
        <Route path="/file/*" element={!token == "" ? <FileView /> : <Navigate to="/" />} />
        <Route path="/user" element={!token == "" ? <Navigate to={isUser() ? "/dashboard" : "/admin"} />  : <Navigate to="/" />} />
        <Route path="/dashboard/*" element={
            token ? (firstTime() ? <Firstvisit /> : <Dashboard />) : <Navigate to="/" />
        } />
        <Route path="/detailsform" element={ <Firstform/>} />
        <Route path="/about" element={ <HeroBullets/>} />
        <Route path="/contact" element={ <GetInTouchSimple/>} />
        {/* <Route path='/doctor' element={<Doctor/>}/> */}
    </Routes>
}