import './App.css'
import { Box, Button, rem } from '@mantine/core'
import './assets/css/vars.css';
import './assets/css/layout.css';
import './assets/css/style.css';
import './assets/css/theme.css';
import { Navbar } from './components/partials/navbar/Navbar.jsx';
import { Footer } from './components/partials/Footer.jsx';
import { AppRoutes } from './routes/AppRoutes.jsx';
import { FileView } from './components/modules/user-dashboard/file-work/FileView.jsx';
import { AddNote } from './components/modules/user-dashboard/file-work/AddNote.jsx';



function App() {
  // const [count, setCount] = useState(0)
  return (
    <>
    <Navbar/>
      {/* <AddNote/> */}
      {/* <FileView/> */}
      <AppRoutes/>
    <Footer/>
    </>
  )
}

export default App
