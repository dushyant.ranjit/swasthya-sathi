const mongoose = require('mongoose');
const Schema = mongoose.Schema

const UserSchema = new Schema({
    title: {
        type: "String",
        required: true,
    },
    path: {
        type: "String",
        required: true,
    },
    user:{
        type: mongoose.Schema.Types.ObjectId,
        required:true,
        ref:'User',
    }
    
}, { timestamps: true });

module.exports = mongoose.model('UserDoc',UserSchema);