const mongoose = require('mongoose');
const Schema = mongoose.Schema

const UserSchema = new Schema({
    doctor:{
        type: mongoose.Schema.Types.ObjectId,
        required:true,
        ref:'User',
    },
    patient:{
        type: mongoose.Schema.Types.ObjectId,
        required:true,
        ref:'User',
    }
}, { timestamps: true });

module.exports = mongoose.model('Access',UserSchema);