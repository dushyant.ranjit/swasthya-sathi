const mongoose = require('mongoose');
const Schema = mongoose.Schema

const UserSchema = new Schema({
    dob: {
        type: "Date",
        required: true,
    },
    weight: {
        type: "Number",
        required: true,
    },
    height: {
        type: "String",
        required: true,
    },
    bloodGroup:{
        type: "String",
        enum: ["A+ve","B+ve","AB+ve","O+ve","A-ve","B-ve","AB-ve","O-ve"],
        required: true,
    },
    gender:{
        type:"String",
        enum:["Male","Female","Other"],
        required:true,
    },
    user:{
        type: mongoose.Schema.Types.ObjectId,
        required:true,
        ref:'User',
        unique:true
    },
    
}, { timestamps: true });

module.exports = mongoose.model('UserDetail',UserSchema);