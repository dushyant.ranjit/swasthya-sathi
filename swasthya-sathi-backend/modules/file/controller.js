const express = require('express');
const multer = require('multer');
const path = require('path');

const router = express.Router();
const fs = require("fs");

const fileStorage = multer.diskStorage({
    // Destination to store image     
    destination: 'uploads/notes', 
      filename: (req, file, cb) => {
          cb(null, file.fieldname + '_' + Date.now() + path.extname(file.originalname))
            // file.fieldname is name of the field (image)
            // path.extname get the uploaded file extension
    }
});

const fileUpload = multer({
      storage: fileStorage,
      limits: {
        fileSize: 1000000 // 1000000 Bytes = 1 MB
      },
      fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(pdf)$/)) { 
           // upload only png and jpg format
           return cb(new Error('Please upload pdf file only one at a time'))
         }
       cb(undefined, true)
    }
}) 

router.get("/:filename", (req, res) => {
  const filePath = path.join(__dirname, `../../uploads/notes/${req.params.filename}`);
  console.log(filePath);
  fs.readFile(filePath, (err, data) => {
    if (err) {
      res.status(500).send("Error reading file");
      return;
    }
    res.send(data);
  });
});

router.post('/upload', fileUpload.single('file'), (req, res) => {
    console.log(req.file);
    res.send(req.file)
}, (error, req, res, next) => {
    res.status(400).send({ error: error.message })
});


module.exports=router;