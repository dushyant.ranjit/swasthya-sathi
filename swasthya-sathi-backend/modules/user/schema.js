const mongoose = require('mongoose');
const Schema = mongoose.Schema

const UserSchema = new Schema({
    fullName: {
        type: "String",
        required: true,
        unique:false
    },
    phone: {
        type: "String",
        required: true,
        // unique:true
    },
    email: {
        unique:true,
        type: "String",
        required: true
    },
    password: {
        type: "String",
        required: true
    },
    userType: {
        type: "String",
        enum: ["admin","client"],
        default: "client"
    },
    role: {
        type: "String",
        enum: ["admin","superadmin","default","doctor","representative"],
        default: "default"
    },
    hasDetails: {
        type: "Bool",
        default: "0"
    },
}, { timestamps: true });

module.exports = mongoose.model('User',UserSchema);


//use User as in u caps for populate because here it is User