// const getAll= (req,res)=>{ 
//     res.send('something')
// }

const schema =require('./schema');

const getAll=async (req,res)=>{ 
    const data = await schema.find({});
    // select * from category;
    res.send({
        status: 200,
        message:'All datas retrieved at getAll',
        data: data
    });
}

const getById = async (req, res) => {
    const data = await schema.findById(req.params.id);
    if (data) {
        res.send({
            status: 200,
            message: 'Data retrieved',
            data: data
        });
    } else {
        res.send({
            status: 404,
            message: 'Data could not be found',
            data: e
        });
    }
}

// const create= (req,res)=>{ 
//     res.send({
//         status: 201,
//         message:'Data created',
//         data: req.body
//     })
// }
const create = async (req, res) => {
    try {
        const data = await schema.create({
           ...req.body
        });
        res.send({
            status: 201,
            message: 'Data created',
            data: data
        })
    } catch (e) {
        res.status(400).send({
            status: 400,
            message: 'Action to create could not be completed',
            data: e
        })
    }
}


const update = async (req, res) => {
    try {
        const data=await schema.findByIdAndUpdate(req.params.id, {
            fullName: req.body.fullName,
            phone: req.body.phone,
            email: req.body.email,
            password: req.body.password,
            userType: req.body.userType,
            role: req.body.role,
            hasDetails: req.body.hasDetails,

        },{
            new:true
        });
        res.send({
            status: 200,
            message: 'Data updated',
            data:data
        })
    } catch (e) {
        res.status(400).send({
            status: 400,
            message: 'Action to update could not be completed',
            data: e.message
        })
    }
}

const remove = async (req, res) => {
    try {
        await schema.findByIdAndDelete(req.params.id);
        res.send({
            status: 200,
            message: 'Data deleted'
        })
    } catch (e) {
        res.status(400).send({
            status: 400,
            message: 'Action to remove could not be completed',
            data: data
        })
    }
}

// const getById= (req,res)=>{ 
//     res.send('something')
// }

module.exports={
    getAll,
    create,
    update,
    remove,
    getById,
}