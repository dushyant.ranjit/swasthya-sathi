//import express from "express"
const express = require("express");
require("dotenv").config();
const cors = require("cors");
const mongoose=require('mongoose');``
const app = express();
app.use(express.json());
app.use(cors());

app.get('',(req,res)=>{
    res.send('Backend Working');
});

const authRouter = require('./modules/authentication/router');
const userRouter = require('./modules/user/router');
const userDetailsRouter = require('./modules/user-details/router');
const userDocsRouter = require('./modules/user-docs/router');
const accessRouter = require('./modules/access/router');
const fileRoute = require('./modules/file/controller'); 

app.use('/file',fileRoute);

app.use('/auth', authRouter);
app.use('/user', userRouter);
app.use('/userdetails', userDetailsRouter);
app.use('/userdocs', userDocsRouter);
app.use('/access', accessRouter);

app.listen(8081,async (req,res)=>{
    console.log('App is running on port 8081');
    try{
        await mongoose.connect(process.env.MONGODB_URL)
        console.log("Database is connected");
    }catch(e){
        console.log(e);
    }
});